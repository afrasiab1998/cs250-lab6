#include <time.h>
#include <iostream>
#include<cstdlib>
#include <ctime>
using namespace std;

int roll()
{
	return 1 + rand() % 6;
}

int main(){
	
	srand(time(NULL));
	int random;
	cout<<"How many times to roll the die: ";
	cin>>random;
	cout<<"Random die sequence: ";
	for (int i=1;i<10;i++){
		random = roll();
		cout << random << " ";
	}
	
}
