#include <time.h>
#include <iostream>
#include<cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
using namespace std;

//swaps two elements of vector by reference
void swapp(vector<int> &v, int j){
	int temp = v[j];
	v[j] = v[j+1];
	v[j+1] = temp; 
}

//random ints from 0 to 100
int random()
{
	return 1 + rand() % 100;
}

int moveMin(vector<int> &in, vector<int> &out){
	
	//copying elements of 'in vector' in 'out vector'
	for (int i = 0; i < in.size()-1; i++){
		out.push_back(in[i]);
	}
	
	//sorting 'out vector'
	for (int i = 0; i < out.size()-1; i++){
		for (int j = 0; j < out.size()-1; j++){
			if (out[j]>out[j+1]){
				swapp(out,j); //swaps elements by reference
			}
		}
	}
	
	//inserts unsorted random number at end
	int end;
	while (true){	
		end = random();
		if (end < out[out.size()-1]){
			out.push_back(end);
			break;
		}
	}
	
	//prints 'out vector'
	cout<<"The Random int to insert after sorting: "<<end<<endl<<endl;
	cout<<"<moveMin> Sorted by my algorithm: \n[ ";
	for (int i = 0; i < out.size(); i++){
		cout<<out[i]<<" ";
	}
	
	return end;
}

//compare both sorted vectors
bool compare(vector<int> &in,vector<int> &out){
	
	bool flag = true;
	for (int i = 0; i < in.size()-1; i++){
		if (in[i] != out[i]){
			flag = false;
		}
		return flag;
	}
	
	}
	
bool testMoveMin(vector<int> &in,vector<int> &out){
	
	//makes vector of 100 random integers
	for (int i = 0; i <100; i++){
		in.push_back(random());
	}
	
	//prints original vector
	cout<<"\n<testMoveMin> Original Random vector:\n[ ";
	for (int i = 0; i < in.size(); i++){
		cout<<in[i]<<" ";
	}
	cout<<"]"<<endl<<endl;
	
	//last element usorted
	int end = moveMin(in,out);
	
	cout<<"]"<<endl<<endl;
	
	//default sort algo
	sort(in.begin(),in.end());
	in.push_back(end);
	
	//prints default sorted vector
	cout<<"<sort> sorted by default algorithm: \n[ ";
	for (int i = 0; i < in.size(); i++){
		cout<<in[i]<<" ";
	}
	
	//comparing fcuntion
	return compare(in,out);
	
}

int main(){
	vector<int> in;
	vector<int> out;
	bool valid = testMoveMin(in,out);
	
	
	cout<<"]\n\nComaparing both sorted vectors by compare() function: \n";
	if (valid == 1){
	cout<<"Comparing Function: Sorted Vectors matching.\n\n";
	}
	
	else{
	cout<<"Comparing Function: Sorted Vectors NOT matching."<<valid;
	}
	
}
