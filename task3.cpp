#include <time.h>
#include <iostream>
#include<cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

//swaps two elements of vector by reference
void swapp(vector<int> &v, int j){
	int temp = v[j];
	v[j] = v[j+1];
	v[j+1] = temp; 
}

//random ints from 0 to 100
int random()
{
	return 1 + rand() % 100;
}

//Quick Sort
int partition(vector<int> & a, int start, int end) {
  unsigned int pivot = a[start];
  unsigned int from_left = start+1;
  unsigned int from_right = end;
  unsigned int tmp;
    
  while (from_left != from_right) {
    if (a[from_left]  <= pivot) from_left++;
    else {
      while (( from_left != from_right)  && (pivot < a[from_right])) from_right--;
      tmp =  a[from_right];
      a[from_right] = a[from_left];
      a[from_left] = tmp;
    }
  }
  
  if (a[from_left]>pivot) from_left--;
  a[start] = a[from_left];
  a[from_left] = pivot;
  
  return (from_left);
}

void quickSort(vector <int> & a, int p, int r) {
  if (p < r) {
    int q = partition(a, p, r);
    quickSort(a, p, q - 1);
    quickSort(a, q + 1, r);
  }
}

int moveMin(vector<int> &in, vector<int> &out){
	
	//copying elements of 'in vector' in 'out vector'
	for (int i = 0; i < in.size()-1; i++){
		out.push_back(in[i]);
	}
	
	//sorting 'out vector' by reference
	quickSort(out, 0, out.size()-1);
	
	//inserts unsorted random number at end
	int end;
	while (true){	
		end = random();
		if (end < out[out.size()-1]){
			out.push_back(end);
			break;
		}
	}	
	return end;
}

int time(clock_t start){
	cout<<double(-clock()+start)/CLOCKS_PER_SEC;
	}
	
void testMoveMin(vector<int> &in,vector<int> &out,int num){
	
	if (num == -1){
		
		vector<double> myAlgo;
		vector<double> defAlgo;
	
		for (int i = 0; i < 100;i++){
			vector<int> temp;
			for (int i = 0; i < 100000; i++){
				temp.push_back(random());
			}
			vector<int> temp1 = temp;
			vector<int> temp2 = temp;
			
			clock_t start = clock();
			quickSort(temp1, 0, temp1.size()-1);
			myAlgo.push_back(double(clock()-start)/CLOCKS_PER_SEC);
			
			clock_t start2 = clock();
			sort(temp2.begin(),temp2.end());
			defAlgo.push_back(double(clock()-start2)/CLOCKS_PER_SEC);

		}
		
		cout<<"\n\nMy Algorithm <Quick sort> stats:\n";
		double max = *max_element(myAlgo.begin(), myAlgo.end());
		cout<<"Best Case Time   : "<<max<<" secs"<<endl;
		double min = *min_element(myAlgo.begin(), myAlgo.end());
		cout<<"Average Case Time: "<<min+((max-min)/2.0)<<" secs"<<endl;
		cout<<"Worst Case Time  : "<<min<<" secs"<<endl;
 		
		cout<<"\nDefault Algorithm <sort> stats:\n";
		double max1 = *max_element(defAlgo.begin(), defAlgo.end());
		cout<<"Best Case Time   : "<<max1<<" secs"<<endl;
		double min1 = *min_element(defAlgo.begin(), defAlgo.end());
		cout<<"Average Case Time: "<<min1+((max1-min1)/2.0)<<" secs"<<endl;
		cout<<"Worst Case Time  : "<<min1<<" secs"<<endl;
 	}
 	
 	else{
	 
	//makes vector of 10,000 random integers
	for (int i = 0; i < num; i++){
		in.push_back(random());
	}
	
	//prints original vector
	cout<<"Integers generated: "<<num;
	
	//last element usorted
	clock_t start = clock();
	int end = moveMin(in,out);
	cout<<"\nMy algorithm <Quick sort>: "<<double(clock()-start)/CLOCKS_PER_SEC<<" secs";
	
	//default sort algo
	clock_t start2 = clock();;
	sort(in.begin(),in.end());
	in.push_back(end);

	cout<<"\nDefault algorithm  <sort>: "<<double(clock()-start2)/CLOCKS_PER_SEC<<" secs\n__________";
	
	}		
}

int main(){
	vector<int> in;
	vector<int> out;
	
	int testCase = 1;
	for (int i = 100; i<= 100000; i = i*10,testCase++){
		cout<<"\n\nTest Case no. "<<testCase<<endl;
		testMoveMin(in,out,i);
	}
	
	cout<<"\n\nRunning 100 times for 100,000 random integers...";
		testMoveMin(in,out,-1);
}
